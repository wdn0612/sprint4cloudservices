package com.hack.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.hack.entities.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    boolean existsByName(String name);
    User findByName(String name);

    @Query(value="select id from user", nativeQuery=true)
    List<Long> getAllIds();

}