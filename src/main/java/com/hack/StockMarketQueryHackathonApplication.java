package com.hack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockMarketQueryHackathonApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockMarketQueryHackathonApplication.class, args);
    }

}
